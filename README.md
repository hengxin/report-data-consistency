# README #

### What is this repository for? ###

This is for my reports on data consistency. 题目: 分布数据一致性及保障技术研究

### Branches ###

* phd-seminar: the ph.d. thesis proposal report
* nasac2015: report for NASAC'2015 at Wuhan; this is a major revision of phd-seminar
* guangzhou2015: report for discussions at Sun Yat-sen University; this is a minor revision of nasac2015

### Contributors ###

* 魏恒峰 ([Hengfeng Wei](http://hengxin.github.io/)) Contact: hengxin0912@gmail.com